import { Routes } from '@angular/router';

import { ActividadComponent } from '../../actividad/actividad.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'actividad',       component: ActividadComponent },
];
