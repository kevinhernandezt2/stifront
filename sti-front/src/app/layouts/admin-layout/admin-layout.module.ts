import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ActividadesService } from '../../servicesdemo/actividades.service';
import { ActividadComponent } from '../../actividad/actividad.component';
import { ModalActividadComponent } from '../../actividad/modal-actividad/modal-actividad.component';


@NgModule({
  imports: [ 
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    NgbModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    ActividadComponent,
    ModalActividadComponent
  ],
  providers: [
    ActividadesService
  ]
})

export class AdminLayoutModule {}
