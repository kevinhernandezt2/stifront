export class ActividadesModel {
    private id: number;
    private nombreActividad: string;
    private estatus: number;
    private fechaHoraEjecucion: string;
    private empleado: number;
  
    constructor(dataActividad:any = {}){
        this.id = dataActividad.id ?  dataActividad.id : null;
        this.nombreActividad = dataActividad.nombreActividad ?  dataActividad.nombreActividad : null;
        this.estatus = dataActividad.estatus ?  dataActividad.estatus : null;
        this.fechaHoraEjecucion = dataActividad.fechaHoraEjecucion ?  dataActividad.fechaHoraEjecucion : null;
        this.empleado = dataActividad.empleado ?  dataActividad.empleado : null;
    }
  }