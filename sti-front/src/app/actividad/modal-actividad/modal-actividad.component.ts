import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActividadesModel } from '../../model/actividades.model';
import { ActividadesService } from '../../servicesdemo/actividades.service';

@Component({
  selector: 'app-modal-actividad',
  templateUrl: './modal-actividad.component.html',
  styleUrls: ['./modal-actividad.component.css']
})
export class ModalActividadComponent implements OnInit {

  @Input() titulo: string = 'Agregar Actividad';
  @Input() isEdit: boolean = false;
  @Input() dataActividad: ActividadesModel | null = null;
  @Output() sendDataForm: EventEmitter<ActividadesModel> = new EventEmitter<ActividadesModel>();

  estatus: any = [];
  empleados: any = [];
  

  public formulario: FormGroup = new FormGroup({
    id: new FormControl({ value: null, disabled: false }, []),
    nombreActividad: new FormControl({ value: null, disabled: false }, [Validators.required, Validators.min(3)]),
    estatus: new FormControl({ value: null, disabled: false }, [Validators.required]),
    fechaHoraEjecucion: new FormControl({ value: null, disabled: false }, [Validators.required]),
    empleado: new FormControl({ value: null, disabled: false }, [
      Validators.required,
    ]),
  });

  constructor(private activeModal: NgbActiveModal, private actividadesService: ActividadesService) { 
  }

  ngOnInit(): void {

    this.actividadesService.getEstatus().subscribe( data => {
      this.estatus = data.respuesta;
    });

    this.actividadesService.getEmpleados().subscribe( data => {
      this.empleados = data.respuesta;
    });

    if(this.isEdit){
      this.formulario.reset({ ...this.dataActividad });
    }
  }

  onCancelar(): void {
    this.activeModal.close();
  }

  onCrearModificarActividad():void{

    const datepipe: DatePipe = new DatePipe('en-US')
    let formattedDate = datepipe.transform(this.formulario.controls['fechaHoraEjecucion'].value, 'yyyy-MM-dd HH:mm:ss')

    let obj = {
     "nombreActividad":  this.formulario.controls['nombreActividad'].value,
     "estatus": this.formulario.controls['estatus'].value,
     "fechaHoraEjecucion": formattedDate,
     "empleado":  this.formulario.controls['empleado'].value,
    }

    if (this.formulario.valid) {
      this.sendDataForm.emit(new ActividadesModel(obj));
      this.onCancelar();
    }
  
  }

}
