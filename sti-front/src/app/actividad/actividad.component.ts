import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ActividadesModel } from '../model/actividades.model';
import { ActividadesService } from '../servicesdemo/actividades.service';
import { ModalActividadComponent } from './modal-actividad/modal-actividad.component';

@Component({
  selector: 'app-actividad',
  templateUrl: './actividad.component.html',
  styleUrls: ['./actividad.component.css']
})
export class ActividadComponent implements OnInit {

  private modalRef: NgbModalRef | null = null;
  
  actividades: any = [];

  constructor(private actividadesService: ActividadesService ,private modalService: NgbModal) {
    
   }

  ngOnInit(): void {
    this.actividadesService.getActividades().subscribe( data => {
      this.actividades = data.respuesta;
    })
  
  }

  onCrearActividad(): void {
    
    this.modalRef = this.modalService.open( ModalActividadComponent, {
      backdrop: 'static',
      keyboard: false
    });

    this.modalRef.componentInstance.titulo = 'Registrar actividades';
    this.modalRef.componentInstance.sendDataForm.subscribe(async (actividad: ActividadesModel) => {
  
      this.actividadesService.postCrearActividad(actividad).subscribe(data => {
        
        if (data.mensaje === "actividad ha sido creado con exito!") {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: data.mensaje,
            showConfirmButton: false,
            timer: 1500
          })
          this.actividadesService.getActividades().subscribe( data => {
            this.actividades = data.respuesta;
          });
        }else{
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: data.mensaje + ' ' + data.error,
            showConfirmButton: false,
            timer: 1500
          })
        }
        
      });
      
    });
}

  onEditarActividad(id:number, nombreActividad:string, estatus:number, fechaHoraEjecucion:string, empleado:number): void {

        let dataActividad = {
          "id": id,
          "nombreActividad": nombreActividad,
          "estatus": estatus,
          "fechaHoraEjecucion": fechaHoraEjecucion,
          "empleado": empleado
        }

    
      this.modalRef = this.modalService.open( ModalActividadComponent, {
        backdrop: 'static',
        keyboard: false
      });
    
      this.modalRef.componentInstance.titulo = 'Editar Actividad';
      this.modalRef.componentInstance.isEdit = true;
      this.modalRef.componentInstance.dataActividad = dataActividad;
      this.modalRef.componentInstance.sendDataForm.subscribe(async (actividad: ActividadesModel) => {

        this.actividadesService.putEditarActividad(actividad, id).subscribe(data => {
        
          if (data.mensaje === "actividad ha sido modificada con exito!") {
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: data.mensaje,
              showConfirmButton: false,
              timer: 1500
            })
            this.actividadesService.getActividades().subscribe( data => {
              this.actividades = data.respuesta;
            });
          }else{
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: data.mensaje + ' ' + data.error,
              showConfirmButton: false,
              timer: 1500
            })
          }
          
        });
        
      });
  }

  onEliminarActividad(id:number){
    
    this.actividadesService.deleteActividad(id).subscribe(data => {
          
      if (data.mensaje === "se elimino la actividad con exito!") {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: data.mensaje,
          showConfirmButton: false,
          timer: 1500
        })
        this.actividadesService.getActividades().subscribe( data => {
          this.actividades = data.respuesta;
        });
      }else{
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: data.mensaje + ' ' + data.error,
          showConfirmButton: false,
          timer: 1500
        })
      }
      
    });
  }
}
