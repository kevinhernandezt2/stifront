import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActividadesService {

  private rutas: {
    actividades: string,
    estatus: string,
    empleados: string,
  }

  constructor(private http: HttpClient) { 
    this.rutas = {
      actividades: "/sti/actividades",
      estatus: "/sti/estatus",
      empleados: "/sti/empleados",
    };
  }

  getActividades(): Observable<any> {
    return this.http.get(`${this.rutas.actividades}/consultar`)
    }

  getEstatus(): Observable<any> {
      return this.http.get(`${this.rutas.estatus}/consultar`)
    }

  getEmpleados(): Observable<any> {
      return this.http.get(`${this.rutas.empleados}/consultar`)
    }

  postCrearActividad(objeto: Object){
      return this.http.post<any>(this.rutas.actividades  + `/crear`, objeto);
    }

  putEditarActividad(objeto: Object, id:number): Observable<any>{
      return this.http.put(this.rutas.actividades  + `/editar/${id}`, objeto);
   
    }

  deleteActividad(id:number):Observable<any> {
      return this.http.delete<any>(this.rutas.actividades + `/eliminar/${id}`);
    }
}
